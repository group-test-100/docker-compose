FROM python:3.6

WORKDIR /usr/src/python-sample-app/
COPY python-sample-app /usr/src/python-sample-app

RUN apt -y update && apt -y upgrade && \
    apt -y install pip && \
    apt -y autoclean && apt -y autoremove  

RUN pip install -r requirements.txt 

#EXPOSE 5000

ENV FLASK_APP=app.py \
    POSTGRESQL_URL=postgresql://worker:worker@/app

COPY . /usr/src/python-sample-app/

RUN flask db upgrade

CMD ["python", "app.py"]
